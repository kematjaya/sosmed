$(function() {
    $('.autocomplete').autocomplete({
        source: function (request, response){
            $.ajax({
                url: this.element.context.attributes.url.value,
                data: { query: request.term },
                success: function (data) {
                    var transformed = $.map(data, function (el) {
                        return {
                            label: el,
                            id: el
                        };
                    });
                    response(transformed);
                },
                error: function () {
                    response([]);
                }
            });
        }
    });
});