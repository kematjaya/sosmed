var handleModals = function() {        
    // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class. 
    $('body').on('hide.bs.modal', function() {
        if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') === false) {
            $('html').addClass('modal-open');
        } else if ($('.modal:visible').size() <= 1) {
            $('html').removeClass('modal-open');
        }
    });

    // fix page scrollbars issue
    $('body').on('show.bs.modal', '.modal', function() {
        if ($(this).hasClass("modal-scroll")) {
            $('body').addClass("modal-open-noscroll");
        }
    });

    // fix page scrollbars issue
    $('body').on('hide.bs.modal', '.modal', function() {
        $('body').removeClass("modal-open-noscroll");
    });

    // remove ajax content and remove cache on modal closed 
    $('body').on('hidden.bs.modal', '.modal:not(.modal-cached)', function () {
        $(this).removeData('bs.modal');
    });
};