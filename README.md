# installation
1. clone project 
    - git clone https://gitlab.com/kematjaya/sosmed.git
2. setup db connection 
    - create file .env.local in root project 
    - add connection string:
        ```
        # postgresql
        #DB_DRIVER=postgresql
        #DB_USER=****
        #DB_PASSWORD=***
        #DB_HOST=127.0.0.1
        #DB_PORT=5432
        #DB_NAME=sosmed
        #DB_VERSION=11

        #mysql
        DB_DRIVER=mysql
        DB_USER=****
        DB_PASSWORD=****
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_NAME=sosmed
        DB_VERSION=5.7
        DATABASE_URL="${DB_DRIVER}://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?serverVersion=${DB_VERSION}&charset=utf8"
        ```
3. update composer 
    ```
    composer update
    ```
4. setup database 
    ```
    // create database
    php bin/console doctrine:database:create

    // update schema
    php bin/console doctrine:schema:update --force
    ```
5. open URL in browser http://ip-address/sosmed/public/index.php 