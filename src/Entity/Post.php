<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"article" = "Article", "image_post" = "ImagePost"})
 */
abstract class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=Member::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $member;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="post", orphanRemoval=true)
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity=Likes::class, mappedBy="post", orphanRemoval=true)
     */
    protected $likes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credential;

    const AUTH_PUBLIC = 'public';
    const AUTH_FRIEND = 'friend';
    const AUTH_ONLY_ME= 'me';
    
    public static function getAuthArrays()
    {
        return [
            self::AUTH_PUBLIC => self::AUTH_PUBLIC,
            self::AUTH_FRIEND => self::AUTH_FRIEND,
            self::AUTH_ONLY_ME => self::AUTH_ONLY_ME
        ];
    }
    
    public static function getAuthIconArrays()
    {
        return [
            self::AUTH_PUBLIC => 'fa-globe',
            self::AUTH_FRIEND => 'fa-users',
            self::AUTH_ONLY_ME => 'fa-user'
        ];
    }
    
    public function getAuthIcon()
    {
        $arr = self::getAuthIconArrays();
        
        return $arr[$this->getCredential()];
    }
    
    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
    }

    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getMember(): ?Member
    {
        return $this->member;
    }

    public function setMember(?Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Likes[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Likes $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setPost($this);
        }

        return $this;
    }

    public function removeLike(Likes $like): self
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getPost() === $this) {
                $like->setPost(null);
            }
        }

        return $this;
    }

    public function getLike(Member $member): Collection
    {
        return $this->getLikes()->filter(function (Likes $like) use ($member){
            return (string)$like->getMember()->getId() === (string) $member->getId();
        });
    }
    
    public function isLikes(Member $member):bool
    {
        $like = $this->getLike($member);
        
        return !$like->isEmpty();
    }

    public function getCredential(): ?string
    {
        return $this->credential;
    }

    public function setCredential(?string $credential): self
    {
        $this->credential = $credential;

        return $this;
    }
}
