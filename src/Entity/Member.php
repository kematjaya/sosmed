<?php

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Kematjaya\User\Entity\KmjUserInterface;

/**
 * @ORM\Entity(repositoryClass=MemberRepository::class)
 */
class Member implements KmjUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=Network::class, mappedBy="member", orphanRemoval=true)
     */
    private $networks;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="member", orphanRemoval=true)
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity=Likes::class, mappedBy="member")
     */
    private $likes;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;

    public function __construct()
    {
        $this->networks = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->likes = new ArrayCollection();
    }

    public function __toString() 
    {
        return $this->getName();
    }
    
    public function getId(): ?\Ramsey\Uuid\UuidInterface
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): KmjUserInterface
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): KmjUserInterface
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): KmjUserInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): KmjUserInterface
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Network[]
     */
    public function getNetworks(): Collection
    {
        return $this->networks;
    }

    public function addNetwork(Network $network): KmjUserInterface
    {
        if (!$this->networks->contains($network)) {
            $this->networks[] = $network;
            $network->setMember($this);
        }

        return $this;
    }

    public function removeNetwork(Network $network): KmjUserInterface
    {
        if ($this->networks->contains($network)) {
            $this->networks->removeElement($network);
            // set the owning side to null (unless already changed)
            if ($network->getMember() === $this) {
                $network->setMember(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): KmjUserInterface
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setMember($this);
        }

        return $this;
    }

    public function removePost(Post $post): KmjUserInterface
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getMember() === $this) {
                $post->setMember(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Likes[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Likes $like): KmjUserInterface
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setMember($this);
        }

        return $this;
    }

    public function removeLike(Likes $like): KmjUserInterface
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getMember() === $this) {
                $like->setMember(null);
            }
        }

        return $this;
    }

    public function getRoles(): ?array
    {
        $roles = $this->roles;
         // guarantee every user at least has ROLE_USER
        $roles[] = self::ROLE_USER;

        return array_unique($roles);
    }

    public function setRoles(array $roles): KmjUserInterface
    {
        $this->roles = $roles;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): KmjUserInterface
    {
        $this->is_active = $is_active;

        return $this;
    }
    
    private $password_old, $password_new, $password_re_new;
    
    public function setPasswordOld(string $password): KmjUserInterface
    {
        $this->password_old = $password;
        
        return $this;
    }
    
    public function getPasswordOld():?string
    {
        return $this->password_old;
    }
    
    public function setPasswordNew(string $password): KmjUserInterface
    {
        $this->password_new = $password;
        
        return $this;
    }
    
    public function getPasswordNew():?string
    {
        return $this->password_new;
    }
    
    public function setPasswordReNew(string $password): KmjUserInterface
    {
        $this->password_re_new = $password;
        
        return $this;
    }
    
    public function getPasswordReNew():?string
    {
        return $this->password_re_new;
    }

    public function eraseCredentials() 
    {
        
    }

    public function getSalt() 
    {
        return $this->email;
    }

    public function likes(Post $post):Likes
    {
        $like = (new Likes())
                ->setCreatedAt(new \DateTime())
                ->setMember($this)->setPost($post);
        
        return $like;
    }
}
