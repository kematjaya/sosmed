<?php

namespace App\Repository;

use App\Entity\Member;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kematjaya\User\Repo\KmjUserRepoInterface;
use Kematjaya\User\Entity\KmjUserInterface;
use Doctrine\ORM\EntityManagerInterface;
/**
 * @method Member|null find($id, $lockMode = null, $lockVersion = null)
 * @method Member|null findOneBy(array $criteria, array $orderBy = null)
 * @method Member[]    findAll()
 * @method Member[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberRepository extends ServiceEntityRepository implements KmjUserRepoInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Member::class);
    }

    public function createUser(): KmjUserInterface 
    {
        return new Member();
    }

    public function findOneByUsernameAndActive(string $username): ?KmjUserInterface 
    {
        return $this->findOneBy(['username' => $username, 'is_active' => true]);
    }
    
    
    public function getSugestedMember(Member $member):array
    {
        $ids = [$member->getId()];
        foreach($member->getNetworks() as $network)
        {
            $ids[] = $network->getMemberNetwork()->getId();
        }
        
        $qb = $this->createQueryBuilder('t');
        $qb->where($qb->expr()->notIn('t.id', $ids))
                ->setMaxResults(10);
        
        return $qb->getQuery()->getResult();
    }
    
    public function followMember(Member $friend, Member $member):void
    {
        $network = (new \App\Entity\Network())
                ->setMember($member)->setMemberNetwork($friend)
                ->setCreatedAt(new \DateTime());
        $this->_em->transactional(function(EntityManagerInterface $em) use($network) {
            $em->persist($network);
        });
    }
}
