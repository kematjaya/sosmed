<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findByMember(\App\Entity\Member $member):array
    {
        $ids = [$member->getId()];
        foreach($member->getNetworks() as $network)
        {
            $ids[] = $network->getMemberNetwork()->getId();
        }
        
        $qb = $this->createQueryBuilder('t')->join('t.member', 'm');
        
        $expr = $qb->expr();
        
        $qb->where(
                $expr->andX()
                    ->add($expr->in('m.id', $ids))
                    ->add($expr->eq('t.credential', $expr->literal(Article::AUTH_FRIEND)))
            )->orWhere(
                $expr->andX()->add($expr->eq('t.credential', $expr->literal(Article::AUTH_PUBLIC)))
            )
            ->orWhere(
                $expr->andX()
                    ->add($expr->eq('t.credential', $expr->literal(Article::AUTH_ONLY_ME)))
                    ->add($expr->eq('m.id', $expr->literal($member->getId())))
            );
        
        return $qb->getQuery()->getResult();
    }
}
