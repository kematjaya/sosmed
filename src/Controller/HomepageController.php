<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Post;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class HomepageController extends AbstractController 
{
    private $tokenStorage;
    
    public function __construct(TokenStorageInterface $tokenStorage) 
    {
        $this->tokenStorage = $tokenStorage;
    }
    
    /**
     * @Route("/", name="homepage", methods={"GET", "POST"})
     */
    public function homepage(
            Request $request, 
            ArticleRepository $articleRepo,
            MemberRepository $memberRepo, SluggerInterface $slugger)
    {
        $member = $this->tokenStorage->getToken()->getUser();
        $article = (new Article())
                ->setMember($member)
                ->setCreatedAt(new \DateTime());
        $form = $this->createForm(ArticleType::class, $article);
        
        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if($form->isValid())
            {
                $manager = $this->getDoctrine()->getManager();
                try{
                    $object = $form->getData();
                    $images = $form->get('images')->getData();
                    if ($images) 
                    {
                        $originalFilename = pathinfo($images->getClientOriginalName(), PATHINFO_FILENAME);
                        $safeFilename = $slugger->slug($originalFilename);
                        $newFilename = $safeFilename.'-'.uniqid().'.'.$images->guessExtension();
                        
                        $images->move(
                            $this->getParameter('uppload_dir'),
                            $newFilename
                        );

                        // updates the 'brochureFilename' property to store the PDF file name
                        // instead of its contents
                        $object->setImages($newFilename);
                    }
                    
                    
                    $manager->transactional(function (EntityManagerInterface $em) use ($object) {
                        $em->persist($object);
                    });
                    
                    $this->addFlash("info", 'data berhasil di post');
                    
                    return $this->redirectToRoute('homepage');
                    
                } catch (\Exception $ex) 
                {
                    $this->addFlash("error", $ex->getMessage());
                }
            }else
            {dump($form->getErrors());exit;
                $this->addFlash("error", 'invalid value');
            }
        }
        
        return $this->render('pages/homepage/index.html.twig', [
            'form' => $form->createView(), 'datas' => $articleRepo->findByMember($member),
            'sugested' => $memberRepo->getSugestedMember($member)
        ]);
    }
    
    private function processForm(\Symfony\Component\Form\FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted())
        {
            if($form->isValid())
            {
                $manager = $this->getDoctrine()->getManager();
                try{
                    $object = $form->getData();
                    $manager->transactional(function (EntityManagerInterface $em) use ($object) {
                        $em->persist($object);
                    });
                    
                    $this->addFlash("info", 'data berhasil di post');
                    
                    return $object;
                    
                } catch (\Exception $ex) 
                {
                    $this->addFlash("error", $ex->getMessage());
                }
            }else
            {
                $this->addFlash("error", 'invalid value');
            }
        }
        
        return null;
    }
    
    /**
     * @Route("/{id}/follow", name="app_follow")
     */
    public function follow(\App\Entity\Member $friend, MemberRepository $memberRepo)
    {
        $member = $this->tokenStorage->getToken()->getUser();
        try{
            $memberRepo->followMember($friend, $member);
            $this->addFlash('info', 'success');
        } catch (\Exception $ex) {
            $this->addFlash('error', $ex->getMessage());
        }
            
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("/{id}/likes", name="app_likes")
     */
    public function likes(Post $post)
    {
        $member = $this->tokenStorage->getToken()->getUser();
        $manager = $this->getDoctrine()->getManager();
        try{
            if($post->isLikes($member))
            {
                $like = $post->getLike($member)->first();
                $manager->remove($like);
            }else
            {
                $like = $member->likes($post);
                $manager->persist($like);

                $post->addLike($like);
                $manager->persist($post);
            }
                
            $manager->flush();
            
            $this->addFlash('info', 'like success');
        } catch (\Exception $ex) 
        {
            $this->addFlash('error', $ex->getMessage());
        }
        
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("/{id}/comment", name="app_comment")
     */
    public function comment(Request $request, Post $post)
    {
        $member = $this->tokenStorage->getToken()->getUser();
        $comment = (new Comment())
                ->setMember($member)
                ->setCreatedAt(new \DateTime())
                ->setPost($post);
        
        $form = $this->createForm(CommentType::class, $comment);
        
        $result = $this->processForm($form, $request);
        if($result)
        {
            return $this->redirectToRoute('homepage');
        }
        
        return $this->render('pages/homepage/comment-form.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/{id}/delete", name="app_post_delete", methods={"DELETE"})
     */
    public function postDelete(Post $post, Request $request)
    {
        $this->doDelete($request, $post);
        return $this->redirectToRoute('homepage');
    }
    
    protected function doDelete(Request $request, $object)
    {
        if ($this->isCsrfTokenValid('delete' . $object->getId(), $request->request->get('_token'))) 
        {
            $manager = $this->getDoctrine()->getManager();
            try{
                $manager->transactional(function (EntityManagerInterface $em) use ($object) {
                    $em->remove($object);
                });
                
                if($object->getId())
                {
                    $qb = $em->createQueryBuilder('t')->delete(get_class($object), 'obj')->where('obj.id = :id')
                        ->setParameter('id', $object->getId());
                    $qb->getQuery()->execute();
                }
                
                $this->addFlash('info', 'deleted successfull');
            } catch (\Exception $ex) 
            {
                $this->addFlash('error', $ex->getMessage());
            }
        } else 
        {
            $this->addFlash('error', 'csrf token detected.');
        }
    }
}
