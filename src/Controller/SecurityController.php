<?php

namespace App\Controller;

use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use App\Entity\Member;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SecurityController extends AbstractController 
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(
            MemberRepository $memberRepo, Request $request,
            EncoderFactoryInterface $encoderFactory)
    {
        if($request->getMethod() == Request::METHOD_POST)
        {
            if($this->isCsrfTokenValid('register', $request->request->get('_token')))
            {
                $manager = $this->getDoctrine()->getManager();
                $manager->beginTransaction();
                try{
                    $member = $memberRepo->findOneBy(['username' => trim($request->request->get('username'))]);
                    if($member)
                    {
                        throw new \Exception('username already use');
                    }
                    
                    $member = $memberRepo->findOneBy(['email' => trim($request->request->get('email'))]);
                    if($member)
                    {
                        throw new \Exception('email already use');
                    }
                    
                    $member = $memberRepo->createUser();
                    $member->setEmail(trim($request->request->get('email')));
                    $member->setName(trim($request->request->get('name')));
                    $member->setUsername(trim($request->request->get('username')));
                    $member->setIsActive(true);
                    
                    $encoder = $encoderFactory->getEncoder($member);
                    $password = $encoder->encodePassword( trim($request->request->get('password')), $member->getSalt());
                    $member->setPassword($password);
                    $member->setRoles([Member::ROLE_USER]);
                    
                    $manager->persist($member);
                    $manager->flush();
                    $manager->commit();
                    
                    $this->addFlash('info', 'register successfull');
                    
                    return $this->redirectToRoute('kmj_user_login');
                } catch (\Exception $ex) {
                    $manager->rollBack();
                    $this->addFlash('error', $ex->getMessage());
                }
                    
            }else
            {
                $this->addFlash('error', 'csrf token detected.');
            }
            
        }
        return $this->render('pages/security/register.html.twig', [
            'error' => null, 'username' => $request->request->get('username'),
            'email' => $request->request->get('email'), 'name' => $request->request->get('name')
        ]);
    }
}
