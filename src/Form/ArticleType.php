<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Shapecode\Bundle\HiddenEntityTypeBundle\Form\Type\HiddenEntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class ArticleType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at', Type\HiddenDateTimeType::class)
            ->add('images', FileType::class, [
                'label' => 'images',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpg', 'image/png', 'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid images',
                    ])
                ]
            ])
            ->add('content', null, [
                'label' => 'content'
            ])
            ->add('credential', ChoiceType::class, [
                'label' => 'authorization',
                'choices' => array_flip(Article::getAuthArrays())
            ])
            ->add('member', HiddenEntityType::class, [
                'class' => \App\Entity\Member::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
