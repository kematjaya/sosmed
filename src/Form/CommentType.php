<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Shapecode\Bundle\HiddenEntityTypeBundle\Form\Type\HiddenEntityType;
use App\Form\Type\HiddenDateTimeType;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('created_at', HiddenDateTimeType::class)
            ->add('content', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, [
                'required' => true
            ])
            ->add('post', HiddenEntityType::class, [
                'class' => \App\Entity\Post::class
            ])
            ->add('member', HiddenEntityType::class, [
                'class' => \App\Entity\Member::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
